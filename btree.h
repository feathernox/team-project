#pragma once
#include <iostream>
#include <memory>
#include <stack>
#include <exception>
#include <fstream>
#include <cassert>

#include "node.h"

template <class Key, class Value> class Node;
template <class Key, class Value> class Btree;
template <class Key, class Value> struct NodeOfList;

template <class Key1, class Value1>
class Iterator {
public:
    friend class Btree<Key1, Value1>;
    friend class Node<Key1, Value1>;

    Iterator() {}

    Iterator(std::shared_ptr<NodeOfList<Key1, Value1>> nodeOfList,
             Btree<Key1, Value1>* tree):
        objectPtr(nodeOfList), btree(tree){}
    std::pair<Key1, Value1>& operator*() {
        auto res =
                std::make_shared<std::pair<Key1, Value1>>
                (std::make_pair(objectPtr->key, objectPtr->value));
        return *res;
    }
    std::shared_ptr<std::pair<Key1, Value1> > operator->() {
        return objectPtr;
    }

    Iterator& operator++() {
        if (*this == btree->end()) {
            throw(std::logic_error("Trying to increment end iterator"));
        }
        auto current = btree->findKey(this->objectPtr->key);
        assert(current != nullptr);
        if (current->isLeaf()) {
            if (current->keys.back().key == objectPtr->key) {
                *objectPtr = *current->placeOfBinding;
                return *this;
            }
            for (auto it = current->keys.begin(); it != current->keys.end(); ++it) {
                if (it->key > objectPtr->key) {
                    *objectPtr = *it;
                    return *this;
                }
            }
        }

        if (current->keys.back().key == objectPtr->key) {
            if (current->rightestChild == nullptr) {
                *objectPtr = *current->placeOfBinding;
                return *this;
            }
            current = current->rightestChild;
        } else {
            for (auto it = current->keys.begin(); it != current->keys.end(); ++it) {
                if (it->key > objectPtr->key) {
                    if (it->leftChild != nullptr) {
                        current = it->leftChild;
                         break;
                    }
                }
            }
        }
        while (!current->isLeaf()) {
            current = current->keys.begin()->leftChild;
        }
        *objectPtr = *current->keys.begin();
        return *this;
    }

    Iterator& operator--() {
        if (*this == btree->begin()) {
            throw(std::logic_error("Trying to decrement begin iterator"));
        }
        auto current = btree->findKey(objectPtr->key);
        if (current->isLeaf()) {
            if (current->keys.front().key == objectPtr->key) {
                *objectPtr = *(--current->placeOfBinding);
                return *this;
            }
            for (auto it = current->keys.rbegin(); it != current->keys.rend(); ++it) {
                if (it-> key < objectPtr->key) {
                    *objectPtr = *it;
                    return *this;
                }
            }
        }
        for (auto it = current->keys.begin(); it != current->keys.end(); ++it) {
            if (it->key == objectPtr->key) {
                current = it->leftChild;
                break;
            }
        }
        while (!current->isLeaf()) {
            current = current->rightestChild;
        }
        *objectPtr = current->keys.back();
        return *this;
    }

    Key1 first() {
        if (objectPtr != nullptr) {
            return objectPtr->key;
        } else {
            throw(std::logic_error("Object is not exists, can't return first"));
        }
    }
    Value1 second() {
        if (objectPtr != nullptr) {
            return objectPtr->value;
        } else {
            throw(std::logic_error("Object is not exists, can't return second"));
        }
    }

    bool operator!=(const Iterator& other) {
        if (objectPtr != other.objectPtr) return true;
        return false;
    }
    bool operator==(const Iterator& other) {
        if (objectPtr == other.objectPtr) return true;
        return false;
    }

private:
    std::shared_ptr<NodeOfList<Key1, Value1>> objectPtr;
    Btree<Key1, Value1>* btree;
};

template <class Key, class Value>
struct NodeOfList {
    NodeOfList(){}
    NodeOfList(Key _key, Value _value):
        key(_key), value(_value), leftChild(nullptr) {}
    NodeOfList(Key _key, Value _value,
               std::shared_ptr<Node<Key, Value> > left):
        key(_key), value(_value), leftChild(left){}
    NodeOfList(const NodeOfList& other):
        key(other.key), value(other.value), leftChild(other.leftChild){
    }
    Key key;
    Value value;
    std::shared_ptr<Node<Key, Value> > leftChild;
};

template <class Key, class Value>
class Node {
public:
    friend class Btree<Key, Value>;
    friend class Iterator<Key, Value>;

    Node(){}
    Node(const NodeOfList<Key, Value>& first) {
        keys.push_back(first);
    }
    Node(std::shared_ptr<NodeOfList<Key, Value>> first) {
        keys.push_back(*first);
    }

    ~Node() {}

    bool isLeaf() {
        for (auto it = keys.begin(); it != keys.end(); ++it) {
            if (it->leftChild != nullptr) return false;
        }
        return true;
    }
    const std::list<NodeOfList<Key, Value>>& getKeys() const {
        return keys;
    }

    typename std::list<NodeOfList<Key, Value>>::iterator
    addKeyToList(const Key& _key, const Value& _value, Btree<Key, Value>* btree) {
        std::shared_ptr<NodeOfList<Key, Value>> temp =
                std::make_shared<NodeOfList<Key, Value>>(_key, _value);
        Iterator<Key, Value> result(temp, btree);
        if (btree->greatest.objectPtr->key < temp->key){
            btree->greatest = result;
        }
        if (btree->least.objectPtr->key > temp->key){
            btree->least = result;
        }
        if (keys.back().key < _key) {
            keys.push_back(*temp);
            return --keys.end();
        }
        for (auto it = keys.begin(); it != keys.end(); ++it) {
            if (it->key > _key) {
                keys.insert(it, *temp);
                return it;
            }
        }
        return keys.end();
    }

    typename std::list<NodeOfList<Key, Value>>::iterator
    addKeyToList(const std::shared_ptr<NodeOfList<Key, Value>> temp, Btree<Key, Value>* btree) {
        if (btree->greatest.objectPtr->key < temp->key){
            btree->greatest = Iterator<Key, Value>(temp, btree);
        }
        if (btree->least.objectPtr->key > temp->key){
            btree->least = Iterator<Key, Value>(temp, btree);
        }
        if (keys.back().key < temp->key) {
            keys.push_back(*temp);
            return --keys.end();
        }
        for (auto it = keys.begin(); it != keys.end(); ++it) {
            if (it->key > temp->key) {
                auto res = keys.insert(it, *temp);
                return res;
            }
        }
        return this->keys.end();
    }

    std::pair<Key, Value>& deleteKey(int pos) {
        int count;
        for (auto it = keys.begin(); it != keys.end() && count <= pos; ++it, count++) {
            if (count == pos) {
                std::pair<Key, Value> deleted = std::make_pair(it->key, it->value);
                keys.erase(it);
                return deleted;
            }
        }
    }

    typename std::list<NodeOfList<Key, Value> >::iterator middle() {
        int count = 0;
        int pivot = keys.size() / 2;
        for (auto it = keys.begin(); it != keys.end(); ++it, count++) {
            if (count == pivot) {
                return it;
            }
        }
        return keys.end();
    }

private:
    std::list<NodeOfList<Key, Value>> keys;
    std::shared_ptr<Node> rightestChild;
    std::shared_ptr<Node> parent;
    typename std::list<NodeOfList<Key, Value> >::iterator placeOfBinding;

    void normaliseSize(int deg, std::shared_ptr<Node<Key, Value>> start) {
        auto itRight = placeOfBinding;
        if (itRight != parent->keys.end()
            && ((++itRight != parent->keys.end()
                 && (int)itRight->leftChild->keys.size() > deg - 1)
        || (parent->rightestChild != nullptr
            && parent->rightestChild->keys.size() > deg - 1
            &&itRight == parent->keys.end()))){
            //std::cout << "We rotate right" << std::endl;
            keys.push_back(*placeOfBinding);
            std::shared_ptr<Node<Key, Value>> right;
            if (itRight != parent->keys.end()) {
                right =  itRight->leftChild;
            } else {
                right = parent->rightestChild;
            }
            *placeOfBinding = right->keys.front();
            placeOfBinding->leftChild = start;
            keys.back().leftChild = rightestChild;
            auto beginLeftChild = right->keys.begin()->leftChild;
            if (beginLeftChild != nullptr) {
                beginLeftChild->parent = start;
                rightestChild = beginLeftChild;
                beginLeftChild->placeOfBinding = keys.end();
            } else {
                rightestChild = nullptr;
            }
            right->keys.pop_front();

        } else {
            auto itLeft = placeOfBinding;
            if (itLeft != parent->keys.begin()
            && (--itLeft)->leftChild != nullptr
            && (int)itLeft->leftChild->keys.size() > deg - 1) {
                //std::cout << "We rotate left" << std::endl;
                keys.push_front(*itLeft);
                auto lCursor = itLeft->leftChild;
                *itLeft = lCursor->keys.back();
                keys.front().leftChild = lCursor->rightestChild;
                if (keys.front().leftChild != nullptr) {
                    keys.front().leftChild->parent = start;
                    keys.front().leftChild->placeOfBinding = keys.begin();
                }
                lCursor->keys.pop_back();
                lCursor->rightestChild = itLeft->leftChild;
                if (lCursor->rightestChild != nullptr) {
                    lCursor->rightestChild->placeOfBinding = lCursor->keys.end();
                }
            } else {
                if (placeOfBinding != parent->keys.end()) {
                    //std::cout << "We are merging this and right" << std::endl;
                    ++placeOfBinding;
                    auto rCursor = (placeOfBinding != parent->keys.end()) ?
                                placeOfBinding->leftChild:
                                parent->rightestChild;
                    placeOfBinding->leftChild = start;
                    if (rCursor != nullptr){
                        keys.push_back(*(--placeOfBinding));
                        keys.begin()->leftChild = rightestChild;
                        parent->keys.erase(placeOfBinding);
                        keys.insert(keys.end(),
                                rCursor->keys.begin(), rCursor->keys.end());
                        placeOfBinding = rCursor->placeOfBinding;
                        if (rCursor->rightestChild != nullptr) {
                            for (auto& key: rCursor->keys) {
                                    key.leftChild->parent = start;
                            }
                            rCursor->rightestChild->parent = start;
                        }
                    }
                } else {
                    assert(placeOfBinding != parent->keys.begin());
                    //std::cout << "We are merging this and left" << std::endl;
                        --placeOfBinding;
                        auto lCursor = placeOfBinding->leftChild;
                        ++placeOfBinding;
                        if (placeOfBinding == parent->keys.end()) {
                            parent->rightestChild = lCursor;
                        } else {
                            placeOfBinding->leftChild = lCursor;
                        }
                        if (lCursor != nullptr){
                            lCursor->keys.push_back(*std::prev(placeOfBinding));
                            lCursor->keys.back().leftChild = lCursor->rightestChild;
                            parent->keys.erase(std::prev(placeOfBinding));
                            lCursor->keys.insert(lCursor->keys.end(),
                                   keys.begin(), keys.end());
                            lCursor->placeOfBinding = placeOfBinding;
                            if (rightestChild != nullptr) {
                                for (auto& key: keys) {
                                        key.leftChild->parent = lCursor;
                                }
                                rightestChild->parent = lCursor;
                            }
                            lCursor->rightestChild = rightestChild;
                        }
                    }
                }

        }
        if ((int)parent->keys.size() <= deg - 2) {
            if (parent->parent != nullptr) {
                parent->normaliseSize(deg, parent);
            }
        }
    }
};

template <class Key, class Value>
class Btree {
public:
    Btree(){
      root = nullptr;
      elementCount = 0;
    }

    Btree(const Btree &other) {
        root = other.root;
        least = other.least;
        greatest = other.greatest;
        elementCount = other.elementCount;
        deg = other.deg;
    }

    friend class Iterator<Key, Value>;
    friend class Node<Key, Value>;

    using iterator = Iterator<Key, Value>;
    using const_iterator = Iterator<const Key, const Value>;
    iterator begin(){
        return least;
    }

    iterator end(){
        return greatest;
    }

    size_t size(){
        return (size_t)elementCount;
    }


    Value& operator[](const Key& key) {
        auto current = findKey(key);
        if (current == nullptr) {
            throw(std::logic_error("Trying to return value of unexisting object"));
        } else {
            for (auto it = current->keys.begin(); it != current->keys.end(); ++it) {
                if (it->key == key) {
                    return it->value;
                }
            }
        }
    }

    bool find (const Key& key) {
        auto res = findKey(key);
        if (res == nullptr) {
            return false;
        }
        for (auto it = res->keys.begin(); it != res->keys.end(); ++it) {
            if (it->key == key) {
                return true;
            }
        }
        assert(false);
    }

    void insert(const Key& _key, const Value& _value) {
        //do next if not found
        elementCount++;
        if (root == nullptr) {
            std::shared_ptr<NodeOfList<Key, Value>> temp =
                    std::make_shared<NodeOfList<Key, Value>>(_key, _value);
            Node<Key, Value> node(temp);
            root = std::make_shared<Node<Key, Value>>(node);
            least = Iterator<Key, Value>(temp, this);
            greatest = Iterator<Key, Value>(temp, this);
            return;
        }
        std::shared_ptr<Node<Key, Value>> cursor = root;
        int leafcount = 0;
        while (!cursor->isLeaf() || leafcount <= 1) {
            if (cursor->isLeaf()){
                leafcount++;
            }
            for (auto it = cursor->keys.begin(); it != cursor->keys.end(); ++it) {
                if (it->key == _key) {
                    throw (std::logic_error("Trying to insert an existing object"));
                }
            }
            if (cursor->keys.size() >= 2 * deg - 1) {
                 auto divider = cursor->middle();
                 auto leftPart = std::make_shared<Node <Key, Value>>();
                 auto rightPart = std::make_shared<Node <Key, Value>>();
                 leftPart->keys.splice(leftPart->keys.end(), cursor->keys,
                     cursor->keys.begin(), divider);
                 rightPart->keys.splice(rightPart->keys.end(), cursor->keys,
                     ++cursor->keys.begin(), cursor->keys.end());

                 rightPart->rightestChild = cursor->rightestChild;

                 if (cursor->parent != nullptr){
                     divider =
                             cursor->parent->addKeyToList(
                                 std::make_shared<NodeOfList<Key, Value>>(*divider), this);
                     cursor->keys.clear();
                     cursor = cursor->parent;
                 } else {
                     root = cursor;
                 }

                 if (cursor->parent != nullptr){
                     leftPart->parent = cursor->parent;
                     rightPart->parent = cursor->parent;

                 }
                 else{
                     leftPart->parent = cursor;
                     rightPart->parent = cursor;
                 }

                 leftPart->rightestChild = divider->leftChild;
                 if (leftPart->rightestChild != nullptr) {
                    leftPart->rightestChild->parent = leftPart;
                 }
                 for (auto& key: leftPart->keys) {
                     if (key.leftChild != nullptr) {
                        key.leftChild->parent = leftPart;
                     }
                 }
                 leftPart->placeOfBinding = divider;
                 divider->leftChild = leftPart;
                 ++divider;
                 if (rightPart->rightestChild != nullptr) {
                    rightPart->rightestChild->parent = rightPart;
                 }
                 for (auto& key: rightPart->keys) {
                     if (key.leftChild != nullptr) {
                        key.leftChild->parent = rightPart;
                     }
                 }
                 rightPart->placeOfBinding = divider;
                 --divider;
                 divider->leftChild = leftPart;

                 ++divider;
                 if (divider == cursor->keys.end()){
                     cursor->rightestChild = rightPart;
                 } else {
                     divider->leftChild = rightPart;
                 }
                 --divider;
                 if (divider->key > _key){
                     cursor = leftPart;
                 }
                 else{
                     cursor = rightPart;
                 }
             }


            if (!cursor->isLeaf()) {
                if (cursor->keys.back().key < _key) {
                    cursor = cursor->rightestChild;
                } else {
                    for (auto it = cursor->keys.begin(); it != cursor->keys.end(); ++it) {
                        if (it->key > _key) {
                            cursor = it->leftChild;
                            break;
                        }
                    }
                }
            }

             if (leafcount == 2){
                break;
             }
        }
        cursor->addKeyToList(_key, _value, this);
    }


    void print(){
        graphicPrint(root.get(), "");
    }
    void erase(const Key& _key) {
        if (root->isLeaf()) {
            for (auto it = root->keys.begin();
                 it != root->keys.end(); ++it) {
                if (it->key == _key) {
                    elementCount--;
                    root->keys.erase(it);
                    return;
                }
            }
        }
        auto cursor = root;
        auto it = cursor->keys.begin();
        while (cursor != nullptr) {
            bool found = false;
            if (cursor->keys.back().key < _key) {
                cursor = cursor->rightestChild;
            } else {
                for (it = cursor->keys.begin(); it != cursor->keys.end(); ++it) {
                    if (it->key == _key) {
                        found = true;
                        break;
                    }
                    if (it->key > _key) {
                        cursor = it->leftChild;
                        break;
                    }
                }
            }
            if (found) {
                break;
            }
        }
        if (cursor == nullptr) {
            throw(std::invalid_argument("Key not found"));
        }
        elementCount--;
        if (_key == least.objectPtr->key) {
            ++least;
        }
        if (_key == greatest.objectPtr->key) {
            --greatest;
        }
        eraseFromPos(*it, cursor);
        if (root->keys.size() == 0) {
            root = root->rightestChild;
        }
    }


private:
    iterator greatest;
    iterator least;
    std::shared_ptr<Node<Key, Value> > root;
    int elementCount;
    static const int deg = 2;
    void eraseFromPos(const NodeOfList<Key, Value>& pos,
                      std::shared_ptr<Node<Key, Value>> cursor ) {
        if (cursor->isLeaf()) {

            for (auto it = cursor->keys.begin(); it != cursor->keys.end(); ++it) {
                if (it->key == pos.key) {
                    cursor->keys.erase(it);
                    break;
                }
            }
            if (cursor->keys.size() <= deg - 2) {
                cursor->normaliseSize(deg, cursor);
            } else {
                return;
            }
        } else {
            //Here cursor IS NOT LEAF!
            auto it = cursor->keys.begin();
            for (it = cursor->keys.begin(); it != cursor->keys.end(); ++it) {
                if (it->key == pos.key) break;
            }

            assert(it->leftChild != nullptr);
            {
                cursor = it->leftChild;
                while (!cursor->isLeaf() && cursor->rightestChild != nullptr) {
                    cursor = cursor->rightestChild;
                }
                std::swap(it->key, cursor->keys.back().key);
                std::swap(it->value, cursor->keys.back().value);
                eraseFromPos(cursor->keys.back(), cursor);
            }
        }
    }

private:
    std::shared_ptr<Node<Key, Value>> findKey(const Key& key) {
        auto cursor = root;
        while (cursor != nullptr) {
            if (cursor->keys.back().key < key) {
                cursor = cursor->rightestChild;
            } else {
                for (auto it = cursor->keys.begin(); it != cursor->keys.end(); ++it) {
                    if (it->key == key) {
                        return cursor;
                    }
                    if (it->key > key) {
                        cursor = it->leftChild;
                        break;
                    }
                }
            }
        }
        return nullptr;
    }



    void static graphicPrint(const Node<Key, Value> *node, std::string s) {
        if (s.length() > 3)
            return;
        if (node == nullptr){
            return;
        }
        for (auto it = node->keys.begin(); it != node->keys.end(); ++it){
            graphicPrint((it->leftChild).get(), s + "\t");
            std::cout << s << "(" << it->key << " ; " << it->value << ")\n";
        }
        graphicPrint((node->rightestChild).get(), s + "\t");
    }
};
