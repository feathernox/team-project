#pragma once

#include <iterator>
#include <memory>
#include <utility>
#include <vector>

#include <cstdlib>

//this number supposed to be prime
const int kDefaultTableSize = 17;
//this supposed to be not more than 1
const double kMaxLoadFactor = 0.5;

enum States {
	kElementAbsence,
	kElementExistence
};

enum NumOfTable {
	kEnd,
	kTableOne,
	kTableTwo
};

template <typename Key, typename Value>
class AssociativeArray {
public:
	AssociativeArray();
	AssociativeArray(int table_size);
	AssociativeArray(const AssociativeArray & other_table);
	AssociativeArray(AssociativeArray && other_table);
	AssociativeArray& operator=(const AssociativeArray & other_table);
	AssociativeArray& operator=(AssociativeArray && other_table);

    class Iterator: public std::iterator <std::forward_iterator_tag, std::pair<Key, Value> > {
        public:
			friend class AssociativeArray;
			Iterator(const Iterator & other);
			Iterator & operator= (const Iterator & other);
			std::pair<Key, Value> & operator*() const;
			std::pair<Key, Value> * operator->() const;
			Iterator& operator ++();
			Iterator operator ++(int);
			bool operator==(const Iterator& other) const;
			bool operator!=(const Iterator& other) const;
        private:
			Iterator(AssociativeArray & table, NumOfTable number,
				const typename std::vector<std::pair<std::pair<Key, Value>, States>>::iterator & iterator);
			AssociativeArray & reference_table_;
			typename std::vector<std::pair<std::pair<Key, Value>, States>>::iterator iterator_;
			NumOfTable number_;
	};

	class ConstIterator : public std::iterator <std::forward_iterator_tag, std::pair<Key, Value> > {
		public:
			friend class AssociativeArray;
			ConstIterator(const Iterator & other);
			ConstIterator(const ConstIterator & other);
			ConstIterator & operator= (const ConstIterator & other);
			const std::pair<Key, Value> & operator*() const;
			const std::pair<Key, Value> * operator->() const;
			ConstIterator& operator ++();
			ConstIterator operator ++(int);
			bool operator==(const ConstIterator& other) const;
			bool operator!=(const ConstIterator& other) const;
		private:
			ConstIterator(const AssociativeArray & table, NumOfTable number,
				const typename std::vector<std::pair<std::pair<Key, Value>, States>>::const_iterator & iterator);
			const AssociativeArray & reference_table_;
			typename std::vector<std::pair<std::pair<Key, Value>, States>>::const_iterator iterator_;
			NumOfTable number_;
	};
	Value & operator[](const Key& ind);
	Value & operator[](Key && ind);
	Iterator begin();
	Iterator end();
	ConstIterator begin() const;
	ConstIterator end() const;
	ConstIterator cbegin() const;
	ConstIterator cend() const;
	Iterator find(const Key& ind);
	ConstIterator find(const Key& ind) const;
	std::pair<Iterator, bool> insert(const std::pair<Key, Value> & element);
	std::pair<Iterator, bool> insert(std::pair<Key, Value> && element);
	Iterator erase(ConstIterator pos);
	int erase(const Key& ind);
	int size() const;
private:
	std::vector<std::pair<std::pair<Key, Value>, States>> table_one_;
	std::vector<std::pair<std::pair<Key, Value>, States>> table_two_;
	int num_elements_;
	std::pair<int, int> func_one_;
	std::pair<int, int> func_two_;
	//these are helper functions
	int GeneratePrime(int base) const;
	int GetHashOne(const Key &ind) const;
	int GetHashTwo(const Key &ind) const;
	void Rehash();
};

template <typename Key, typename Value>
AssociativeArray<Key, Value>::AssociativeArray() :
	num_elements_(0),
	table_one_(kDefaultTableSize),
	table_two_(kDefaultTableSize){
	for (auto it = table_one_.begin();
	it != table_one_.end(); ++it)
		it->second = kElementAbsence;
	for (auto it = table_two_.begin();
	it != table_two_.end(); ++it)
		it->second = kElementAbsence;
	prime_ = 0;
}

template <typename Key, typename Value>
AssociativeArray<Key, Value>::AssociativeArray(int table_size): 
	num_elements_(0){
	if (table_size <= 0)
		table_size = kDefaultTableSize;
	table_one_.resize(table_size);
	table_two_.resize(table_size);
	for (auto it = table_one_.begin();
	it != table_one_.end(); ++it)
		it->second = kElementAbsence;
	for (auto it = table_two_.begin();
	it != table_two_.end(); ++it)
		it->second = kElementAbsence;
}

template <typename Key, typename Value>
AssociativeArray<Key, Value>::AssociativeArray(const AssociativeArray & other_table) :
	table_one_(other_table.table_one_),
	table_two_(other_table.table_two_),
	func_one_(other_table.func_one_),
	func_two_(other_table.func_two_),
	num_elements_(other_table.num_elements_) {}

template <typename Key, typename Value>
AssociativeArray<Key, Value>::AssociativeArray(AssociativeArray && other_table) :
	table_one_(std::move(other_table.table_one_)),
	table_two_(std::move(other_table.table_two_)),
	func_one_(std::move(other_table.func_one_)),
	func_two_(std::move(other_table.func_two_)),
	num_elements_(std::move(other_table.num_elements_)) {}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value> &
	AssociativeArray<Key, Value>::operator=(const AssociativeArray & other_table) {
	table_one_ = other_table.table_one_;
	table_two_ = other_table.table_two_;
	func_one_ = other_table.func_one_;
	func_two_ = other_table.func_two_;
	num_elements_ = other_table.num_elements_;
	return *this;
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value> &
	AssociativeArray<Key, Value>::operator=(AssociativeArray && other_table) {
	table_one_ = std::move(other_table.table_one_);
	table_two_ = std::move(other_table.table_two_);
	func_one_ = std::move(other_table.func_one_);
	func_two_ = std::move(other_table.func_two_);
	num_elements_ = std::move(other_table.num_elements_);
	return *this;
}


template <typename Key, typename Value>
AssociativeArray<Key, Value>::Iterator::Iterator(AssociativeArray & table, NumOfTable number,
	const typename std::vector<std::pair<std::pair<Key, Value>, States>>::iterator & iterator) :
	reference_table_(table),
	number_(number),
	iterator_(iterator){
	if (number_ == kEnd)
		return *this;
	if (number_ == kTableOne) {
		for (; iterator_ != reference_table_.table_one_.end(); ++iterator_) {
			if (iterator_->second == kElementExistence)
				return *this;
		}
		number_ = kTableTwo;
		iterator_ = reference_table_.table_two_.begin();
	}
	for (; iterator_ != reference_table_.table_two_.end(); ++iterator_) {
		if (iterator_->second == kElementExistence)
			return *this;
	}
	number_ = kEnd;
	return *this;
}

template <typename Key, typename Value>
AssociativeArray<Key, Value>::Iterator::Iterator(const Iterator & other):
	reference_table_(other.reference_table_),
	number_(other.number_),
	iterator_(other.iterator_) {}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::Iterator & 
	AssociativeArray<Key, Value>::Iterator::operator= (const Iterator & other) {
	reference_table_ = other.reference_table_;
	iterator_ = other.iterator_;
	number_ = other.number_,
	return *this;
}

template <typename Key, typename Value>
std::pair<Key, Value> & AssociativeArray<Key, Value>::Iterator::operator*() const {
	return iterator_->first;
}

template <typename Key, typename Value>
std::pair<Key, Value> * AssociativeArray<Key, Value>::Iterator::operator->() const {
	return &(iterator_->first);
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::Iterator&
	AssociativeArray<Key, Value>::Iterator::operator++() {
	if (number_ == kEnd)
		return *this;
	if (number_ == kTableOne) {
		if (iterator_ != reference_table_.table_one_.end())
			++iterator_;
		for (; iterator_ != reference_table_.table_one_.end(); ++iterator_)
			return *this;
		number_ == kTableTwo;
	}
	else if (number_ == kTableTwo) {
		++iterator_;
	}
	for (; iterator_ != reference_table_.table_two_.end(); ++iterator_){
		return *this;
	}
	number_ = kEnd;
	return *this;
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::Iterator
	AssociativeArray<Key, Value>::Iterator::operator++(int) {
	Iterator return_iterator = *this;
	++*this;
	return return_iterator;
}

template <typename Key, typename Value>
bool AssociativeArray<Key, Value>::Iterator::operator==(const Iterator& other) const {
	return iterator_ == other.iterator_;
}

template <typename Key, typename Value>
bool AssociativeArray<Key, Value>::Iterator::operator!=(const Iterator& other) const {
	return iterator_ != other.iterator_;
}



template <typename Key, typename Value>
AssociativeArray<Key, Value>::ConstIterator::ConstIterator(const AssociativeArray & table, NumOfTable number,
	const typename std::vector<std::pair<std::pair<Key, Value>, States>>::const_iterator & iterator) :
	reference_table_(table),
	number_(number),
	iterator_(iterator) {
	if (number_ == kEnd)
		return *this;
	if (number_ == kTableOne) {
		for (; iterator_ != reference_table_.table_one_.cend(); ++iterator_) {
			if (iterator_->second == kElementExistence)
				return *this;
		}
		number_ = kTableTwo;
		iterator_ = reference_table_.table_two_.cbegin();
	}
	for (; iterator_ != reference_table_.table_two_.cend(); ++iterator_) {
		if (iterator_->second == kElementExistence)
			return *this;
	}
	number_ = kEnd;
	return *this;
}

template <typename Key, typename Value>
AssociativeArray<Key, Value>::ConstIterator::ConstIterator(const Iterator & other) :
	reference_table_(other.reference_table_),
	number_(other.number_),
	iterator_(other.iterator_) {}

template <typename Key, typename Value>
AssociativeArray<Key, Value>::ConstIterator::ConstIterator(const ConstIterator & other) :
	reference_table_(other.reference_table_),
	number_(other.number_),
	iterator_(other.iterator_) {}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::ConstIterator &
AssociativeArray<Key, Value>::ConstIterator::operator= (const ConstIterator & other) {
	reference_table_ = other.reference_table_;
	iterator_ = other.iterator_;
	number_ = other.number_,
	return *this;
}

template <typename Key, typename Value>
const std::pair<Key, Value> & AssociativeArray<Key, Value>::ConstIterator::operator*() const {
	return iterator_->first;
}

template <typename Key, typename Value>
const std::pair<Key, Value> * AssociativeArray<Key, Value>::ConstIterator::operator->() const {
	return &(iterator_->first);
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::ConstIterator&
AssociativeArray<Key, Value>::ConstIterator::operator++() {
	if (number_ == kEnd)
		return *this;
	if (number_ == kTableOne) {
		if (iterator_ != reference_table_.table_one_.cend())
			++iterator_;
		for (; iterator_ != reference_table_.table_one_.cend(); ++iterator_)
			return *this;
		number_ == kTableTwo;
	}
	else if (number_ == kTableTwo) {
		++iterator_;
	}
	for (; iterator_ != reference_table_.table_two_.cend(); ++iterator_) {
		return *this;
	}
	number_ = kEnd;
	return *this;
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::ConstIterator
AssociativeArray<Key, Value>::ConstIterator::operator++(int) {
	Iterator return_iterator = *this;
	++*this;
	return return_iterator;
}

template <typename Key, typename Value>
bool AssociativeArray<Key, Value>::ConstIterator::operator==(const ConstIterator& other) const {
	return iterator_ == other.iterator_;
}

template <typename Key, typename Value>
bool AssociativeArray<Key, Value>::ConstIterator::operator!=(const ConstIterator& other) const {
	return iterator_ != other.iterator_;
}


template <typename Key, typename Value>
Value & AssociativeArray<Key, Value>::operator[](const Key& ind) {
	Iterator exist = find(ind);
	if (exist != end()) {
		return exist->second;
	}
	Value empty_value;
	Iterator new_element = insert(std::make_pair(ind, empty_value)).first;
	return new_element->second;
}

template <typename Key, typename Value>
Value & AssociativeArray<Key, Value>::operator[](Key&& ind) {
	Iterator exist = find(ind);
	if (exist != end()) {
		return exist->second;
	}
	Value empty_value;
	Iterator new_element = insert(std::make_pair(std::move(ind), empty_value)).first;
	return new_element->second;
}


template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::Iterator
	AssociativeArray<Key, Value>::begin() {
	return Iterator(*this, kTableOne, table_one_.begin());
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::Iterator
	AssociativeArray<Key, Value>::end() {
	return Iterator(*this, kEnd, table_two_.end());
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::ConstIterator
	AssociativeArray<Key, Value>::begin() const {
	return ConstIterator(*this, kTableOne, table_one_.cbegin());
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::ConstIterator
	AssociativeArray<Key, Value>::end() const {
	return ConstIterator(*this, kEnd, table_two_.cend());
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::ConstIterator
	AssociativeArray<Key, Value>::cbegin() const {
	return ConstIterator(*this, kTableOne, table_one_.cbegin());
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::ConstIterator
	AssociativeArray<Key, Value>::cend() const {
	return ConstIterator(*this, kEnd, table_two_.cend());
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::Iterator
	AssociativeArray<Key, Value>::find(const Key& ind) {
	int pos_one = GetHashOne(ind);
	if (table_one_[pos_one].second == kElementExistence &&
		table_one_[pos_one].first.first == ind)
		return Iterator(*this, kTableOne, table_one_.begin() + pos_one);
	int pos_two = GetHashTwo(ind);
	if (table_one_[pos_two].second == kElementExistence &&
		table_one_[pos_two].first.first == ind)
		return Iterator(*this, kTableTwo, table_two_.begin() + pos_two);
	return end();
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::ConstIterator
	AssociativeArray<Key, Value>::find(const Key& ind) const {
	int pos_one = GetHashOne(ind);
	if (table_one_[pos_one].second == kElementExistence &&
		table_one_[pos_one].first.first == ind)
		return ConstIterator(*this, kTableOne, table_one_.cbegin() + pos_one);
	int pos_two = GetHashTwo(ind);
	if (table_one_[pos_two].second == kElementExistence &&
		table_one_[pos_two].first.first == ind)
		return ConstIterator(*this, kTableTwo, table_two_.cbegin() + pos_two);
	return cend();
}

template <typename Key, typename Value>
std::pair<typename AssociativeArray<Key, Value>::Iterator, bool>
	AssociativeArray<Key, Value>::insert(const std::pair<Key, Value> & element) {
	Iterator exist = find(element.first);
	if (exist != end())
		return std::make_pair(exist, false);
	Rehash();
	++num_elements_;
	int pos = GetHashOne(element.first);
	for (; table_[pos].second != kElementAbsence;
		pos = (pos + kLinearProbe) % table_.size()) {
	}
	table_[pos] = std::make_pair(element, kElementExistence);
	return std::make_pair(Iterator(*this, table_.begin() + pos), true);
}

template <typename Key, typename Value>
std::pair<typename AssociativeArray<Key, Value>::Iterator, bool>
	AssociativeArray<Key, Value>::insert(std::pair<Key, Value> && element) {
	Iterator exist = find(element.first);
	if (exist != end())
		return std::make_pair(exist, false);
	Rehash();
	++num_elements_;
	int pos = GetHash(element.first);
	for (; table_[pos].second != kElementAbsence;
	pos = (pos + kLinearProbe) % table_.size()) {
	}
	table_[pos] = std::make_pair(std::move(element), kElementExistence);
	return std::make_pair(Iterator(*this, table_.begin() + pos), true);
}

template <typename Key, typename Value>
int AssociativeArray<Key, Value>::erase(const Key & ind) {
	Iterator exist = find(ind);
	if (exist == end())
		return 0;
	exist.iterator_->second = kElementAbsence;
	--num_elements_;
	return 1;
}

template <typename Key, typename Value>
typename AssociativeArray<Key, Value>::Iterator
	AssociativeArray<Key, Value>::erase(ConstIterator pos) {
	if (pos.iterator_->second == kElementAbsence)
		return end();
	--num_elements_;
	pos.iterator_->second == kElementAbsence;
	return Iterator(*this, pos.number_, pos.iterator_);
}

template <typename Key, typename Value>
int AssociativeArray<Key, Value>::size() const {
	return num_elements_;
}

template <typename Key, typename Value>
int AssociativeArray<Key, Value>::GetHashOne(const Key &ind) const {
	unsigned long long hash_ind = std::hash<Value>(ind);
	hash_ind = a_one_* hash_ind + b_one_;
	hash_ind %= table_.size();
	return hash_ind;
}

template <typename Key, typename Value>
int AssociativeArray<Key, Value>::GetHashTwo(const Key &ind) const {
	unsigned long long hash_ind = std::hash<Value>(ind);
	hash_ind = a_two_* hash_ind + b_two_;
	hash_ind %= table_.size();
	return hash_ind;
}

template <typename Key, typename Value>
void AssociativeArray<Key, Value>::Rehash() {
	AssociativeArray map_copy = *this;
	if ((double)(num_elements_ + 1) / table_.size() >= kMaxLoadFactor) {
		int prime = GeneratePrime(2*table_one_.size());
		*this = AssociativeArray(prime);
	}
	else {
		*this = AssociativeArray(map_copy.table_one_.size());
	}
	func_one_.first = rand() % prime;
	func_one_.second = rand() % prime;
	func_two_.first = rand() % prime;
	func_two_.second = rand() % prime;
	for (auto it = map_copy.begin();
	it != map_copy.end(); ++it)
		this->insert(*it);
}

template <typename Key, typename Value>
int AssociativeArray<Key, Value>::GeneratePrime(int base) const {
	if (base % 2 == 0)
		base += 1;
	while (true) {
		for (int i = 3; i*i < base; i+=2) {
			if (base % i == 0) {
				base += 2;
				break;
			}
		}
		return base;
	}
}