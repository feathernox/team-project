#pragma once
#include <iostream>
#include <memory>

#define red true
#define black false

template <typename T, typename G>
class RBTree
{
public:
	class Node: public std::enable_shared_from_this<Node>{
	private:
		std::shared_ptr<Node> left;
		std::shared_ptr<Node> right;
		std::shared_ptr<Node> parent;
		bool color;
		T key;
		G value;
	public:
		friend class RBTree;
		Node(){
			left = NULL;
			right = NULL;
			parent = NULL;
			color = black;
		}
		Node(std::shared_ptr<Node> left_, std::shared_ptr<Node> right_, std::shared_ptr<Node> parent_, bool color_, T key_, G value_){
			left = left_;
			right = right_;
			parent = parent_;
			color = color_;
			key = key_;
			value = value_;
		}
		const T & first() const{
			return key;
		}
		G & second(){
			return value;
		}
		void print() const{
			std::cout << key << " " << value << std::endl;
		}
	private:
		void print(const std::shared_ptr<const Node> nil) const{
			if (this != nil.get() && this != NULL){
				this->left->print(nil);
				this->print();
				this->right->print(nil);
			}
		}
		//������� ��, �� �� ������ ����� (����� ��� operator=)
		void DeleteAllWithoutColor(std::shared_ptr<Node> nil){
			if (this != nil.get() && this != NULL){
				this->left->DeleteAllWithoutColor(nil);
				this->right->DeleteAllWithoutColor(nil);
				std::shared_ptr<Node> tmp = this->shared_from_this();
				//delete(this);
				if (this->parent != NULL){
					if ((this->parent->left).get() == this)
						this->parent->left = nil;
					else
						this->parent->right = nil;
					this->parent = NULL;
				}
				this->parent = NULL;
			}
		}
		//�������� ��, ��������� �����
		std::shared_ptr<Node> InsertAllWithoutColor(const std::shared_ptr<const Node> nil, const std::shared_ptr<Node> my_nil) {
			if (this != nil.get() && this != NULL){
				std::shared_ptr<Node> tmp = std::make_shared<Node>(left, right, parent, color, key, value);
				tmp->left = this->left->InsertAllWithoutColor(nil, my_nil);
				tmp->right = this->right->InsertAllWithoutColor(nil, my_nil);
				if (tmp->left == nil)
					tmp->left = my_nil;
				else
					tmp->left->parent = tmp;
				if (tmp->right == nil)
					tmp->right = my_nil;
				else
					tmp->right->parent = tmp;
				return tmp;
			}
			return my_nil;
		}
		const size_t size(const std::shared_ptr<const Node> nil) const{
			if (this == nil || this == NULL)
				return 0;
			else
				return this->left->size(nil) + 1 + this->right->size(nil);
		}
		std::shared_ptr<Node> min(const std::shared_ptr<const Node> nil) {
			std::shared_ptr<Node> x = this->shared_from_this();
			while (x->left != nil){
				x = x->left;
			}
			return x;
		}
		std::shared_ptr<Node> max(const std::shared_ptr<const Node> nil) {
			std::shared_ptr<Node> x = this->shared_from_this();
			while (x->right != nil){
				x = x->right;
			}
			return x;
		}
		std::shared_ptr<Node> next(const std::shared_ptr<const Node> nil) {
			std::shared_ptr<Node> x = this->shared_from_this();
			if (this == nil.get() || this == NULL){
				std::cout << "There is no next Node" << std::endl;
				return NULL;
			}
			if (this->right != nil)
				return this->right->min(nil);
			std::shared_ptr<Node> tmp = this->parent;
			while (tmp != NULL && x == tmp->right){
				x = tmp;
				tmp = tmp->parent;
			}
			return tmp;
		}
		std::shared_ptr<Node> prev(const std::shared_ptr<const Node> nil) {
			std::shared_ptr<Node> x = this->shared_from_this();
			if (this == nil.get() || this == NULL){
				std::cout << "There is no prev Node" << std::endl;
				return NULL;
			}
			if (this->left != nil)
				return this->left->max(nil);
			std::shared_ptr<Node> tmp = this->parent;
			while (tmp != NULL && x == tmp->left){
				x = tmp;
				tmp = tmp->parent;
			}
			return tmp;
		}
	};
	class iterator{
	private:
		std::shared_ptr<Node> pointer;
		std::shared_ptr<const Node> nil;
	public:
		friend class RBTree;
		iterator(){
			pointer = NULL;
			nil = NULL;
		}
		iterator(std::shared_ptr<Node> n, const std::shared_ptr<const Node> nil_){
			pointer = n;
			nil = nil_;
		}
		iterator(const iterator & i){
			pointer = i.pointer;
			nil = i.nil;
		}
		virtual Node & operator* () const{
			return *pointer;
		}
		virtual Node & operator* (){
			return *pointer;
		}
		virtual std::shared_ptr<Node> operator-> (){
			return pointer;
		}
		virtual iterator & operator++ (){
			pointer = pointer->next(nil);
			if (pointer == nil)
				pointer = NULL;
			return *this;
		}
		virtual iterator & operator++ (int){
			++(*this);
			return *this;
		}
		virtual iterator & operator-- (){
			pointer = pointer->prev(nil);
			if (pointer == nil)
				pointer = NULL;
			return *this;
		}
		virtual iterator & operator-- (int){
			--(*this);
			return *this;
		}
		virtual bool operator!= (const iterator & i) const{
			return pointer != i.pointer;
		}
		virtual bool operator== (const iterator & i) const{
			return pointer == i.pointer;
		}
	};
	using const_iterator = const iterator;
	class reverse_iterator : public iterator{
	public:
		friend class RBTree;
		reverse_iterator() :iterator(){}
		reverse_iterator(std::shared_ptr<Node> n, const std::shared_ptr<Node> nil_) :iterator(n, nil_){}
		reverse_iterator(const reverse_iterator & i) :iterator(i){}
		virtual reverse_iterator & operator++ (){
			this->pointer = this->pointer->prev(this->nil);
			if (this->pointer == this->nil)
				this->pointer = NULL;
			return *this;
		}
		virtual reverse_iterator & operator++ (int){
			++(*this);
			return *this;
		}
		virtual reverse_iterator & operator-- (){
			this->pointer = this->pointer->next(this->nil);
			if (this->pointer == this->nil)
				this->pointer = NULL;
			return *this;
		}
		virtual reverse_iterator & operator-- (int){
			--(*this);
			return *this;
		}
	};
	iterator begin(){
		return iterator(min(), NIL);
	}
	iterator end(){
		return iterator(NULL, NIL);
	}
	iterator find(const T & key_){
		return iterator(FindNode(key_), NIL);
	}
	const_iterator find(const T & key_) const{
		return iterator(FindNode(key_), NIL);
	}
	reverse_iterator rbegin(){
		return reverse_iterator(max(), NIL);
	}
	reverse_iterator rend(){
		return reverse_iterator(NULL, NIL);
	}
	void insert(const std::pair<T, G> & p){
		InsertNode(p.first, p.second);
	}
	bool erase(const T & key_){
		return DeleteNode(key_);
	}
	G & operator[] (const T & key_){
		std::shared_ptr<Node> tmp = FindNode(key_);
		if (tmp == NIL || tmp == NULL){
			G g;
			return InsertNode(key_, g)->value;
		}
		else
			return tmp->value;
	}
	RBTree(){
		NIL = std::make_shared <Node>();
		NIL->left = NIL;
		NIL->right = NIL;
		root = NIL;
	}
	RBTree(const RBTree & x){
		NIL = std::make_shared <Node>();
		NIL->left = NIL;
		NIL->right = NIL;
		root = x.root->InsertAllWithoutColor(x.NIL, NIL);
	}
	RBTree & operator= (const RBTree & x){
		DeleteAll();
		root = x.root->InsertAllWithoutColor(x.NIL, NIL);
		return *this;
	}
	std::shared_ptr<Node> min() const{
		return root->min(NIL);
	}
	std::shared_ptr<Node> max() const{
		return root->max(NIL);
	}
	void print() {
		root->print(NIL);
	}
	const size_t size() const{
		return root->size(NIL);
	}
	std::shared_ptr<Node> get_root() const{
		return root;
	}
private:
	void LeftRotate(std::shared_ptr<Node> x){
		if (x == NIL || x == NULL){
			std::cout << "LeftRotate: x == NIL or x == NULL" << std::endl;
			return;
		}
		if (x->right == NIL){
			std::cout << "LeftRotate: x->right == NIL" << std::endl;
			return;
		}
		std::shared_ptr<Node> y = x->right;
		/*����� ��������� � ���������� ������ ���������� �*/
		x->right = y->left;
		if (y->left != NIL)
			y->left->parent = x;
		/*������ �������� � ��������� �*/
		y->parent = x->parent;
		if (x->parent != NULL){
			if (x == x->parent->left)
				x->parent->left = y;
			else
				x->parent->right = y;
		}
		else
			root = y;
		/*������ � ����� �������� �*/
		y->left = x;
		x->parent = y;
	}
	void RightRotate(std::shared_ptr<Node> x){
		if (x == NIL || x == NULL){
			std::cout << "RightRotate: x == NIL or x == NULL" << std::endl;
			return;
		}
		if (x->left == NIL){
			std::cout << "RightRotate: x->left == NIL" << std::endl;
			return;
		}
		std::shared_ptr<Node> y = x->left;
		/*������ ��������� � ���������� ����� ���������� �*/
		x->left = y->right;
		if (y->right != NIL)
			y->right->parent = x;
		/*������ �������� � ��������� �*/
		y->parent = x->parent;
		if (x->parent != NULL){
			if (x == x->parent->left)
				x->parent->left = y;
			else
				x->parent->right = y;
		}
		else{
			root = y;
		}
		/*������ � ������ �������� �*/
		y->right = x;
		x->parent = y;
	}
	std::shared_ptr<Node> FindNode(T key_){
		std::shared_ptr<Node> current = root;
		while (current != NIL){
			if (key_ == current->key)
				return current;
			if (key_ < current->key)
				current = current->left;
			else
				current = current->right;
		}
		return NULL;
	}
	std::shared_ptr<Node> InsertNode(T key_, G value_){
		std::shared_ptr<Node> current = root;
		std::shared_ptr<Node> parent = NULL;
		/*������ ���� �������� key_*/
		while (current != NIL){
			parent = current;
			if (key_ == current->key){
				current->value = value_;
				return current;
			}
			if (key_ < current->key)
				current = current->left;
			else
				current = current->right;
		}
		/*�������� Node: key = key_*/
		std::shared_ptr<Node> x = std::make_shared<Node>(NIL, NIL, parent, red, key_, value_);
		current = x;
		/*������� Node � ������*/
		if (parent != NULL){
			if (key_< parent->key)
				parent->left = x;
			else
				parent->right = x;
		}
		else {
			root = x;
		}
		x->color = red;
		/*��������������� ������-������ ������� ��� x*/
		while (x != root && x->parent->color == red){
			if (x->parent == x->parent->parent->left){
				std::shared_ptr<Node> y = x->parent->parent->right;
				/*������� ����*/
				if (y->color == red){
					x->parent->color = black;
					y->color = black;
					x->parent->parent->color = red;
					x = x->parent->parent;
				}
				/*������ ����*/
				else{
					/*������ x ����� ��������*/
					if (x == x->parent->right){
						x = x->parent;
						LeftRotate(x);
					}
					x->parent->color = black;
					x->parent->parent->color = red;
					RightRotate(x->parent->parent);
				}
			}
			/*��������������� ������-������ ������� ��� x*/
			else{
				std::shared_ptr<Node> y = x->parent->parent->left;
				/*������� ����*/
				if (y->color == red){
					x->parent->color = black;
					y->color = black;
					x->parent->parent->color = red;
					x = x->parent->parent;
				}
				/*������ ����*/
				else{
					/*������ x ������ ��������*/
					if (x == x->parent->left){
						x = x->parent;
						RightRotate(x);
					}
					x->parent->color = black;
					x->parent->parent->color = red;
					LeftRotate(x->parent->parent);
				}
			}
		}
		root->color = black;
		return current;
	}
	void deleteFixup(std::shared_ptr<Node> x){
		/*��������������� ������-������ ������� ��� �������� x*/
		while (x != root && x->color == black){
			if (x == x->parent->left){
				std::shared_ptr<Node> w = x->parent->right;
				/*������� ���� � �������������� ������ ����*/
				if (w->color == red){
					w->color = black;
					x->parent->color = red;
					LeftRotate(x->parent);
					w = x->parent->right;
				}
				/*����� � ������ ������� w ������*/
				/*� ������� w ��� ������ ����*/
				if (w->left->color == black && w->right->color == black){
					w->color = red;
					x = x->parent;
				}
				else{
					/*������ ������� w ������, � ����� �������������� �������*/
					if (w->right->color == black){
						w->left->color = black;
						w->color = red;
						RightRotate(w);
						w = x->parent->right;
					}
					/*������ ������ ������ w �������*/
					w->color = x->parent->color;
					x->parent->color = black;
					w->right->color = black;
					LeftRotate(x->parent);
					x = root;
				}
			}
			/*����������*/
			else{
				std::shared_ptr<Node> w = x->parent->left;
				if (w->color == red){
					w->color = black;
					x->parent->color = red;
					RightRotate(x->parent);
					w = x->parent->left;
				}
				if (w->right->color == black && w->left->color == black){
					w->color = red;
					x = x->parent;
				}
				else{
					if (w->left->color == black){
						w->right->color = black;
						w->color = red;
						LeftRotate(w);
						w = x->parent->left;
					}
					w->color = x->parent->color;
					x->parent->color = black;
					w->left->color = black;
					RightRotate(x->parent);
					x = root;
				}
			}
		}
		x->color = black;
	}
	bool DeleteNode(T key_){
		std::shared_ptr<Node> x;
		std::shared_ptr<Node> y;
		std::shared_ptr<Node> z;
		z = FindNode(key_);
		if (z == NIL || z == NULL){
			std::cout << "There is no such Node" << std::endl;
			return false;
		}
		/*y ��� ���������� �� z ������� ��� z � ������� ������*/
		if (z->left == NIL || z->right == NIL)
			y = z;
		else{
			y = z->right;
			while (y->left != NIL)
				y = y->left;
		}
		/*x ������������ ������� y ��� NIL*/
		if (y->left != NIL)
			x = y->left;
		else
			x = y->right;
		/*������ �������� y � z, ���� ��� �����*/
		if (y != z){
			std::swap(z->key, y->key);
			std::swap(z->value, y->value);
		}
		/*������� y*/
		x->parent = y->parent;
		if (y->parent != NULL){
			if (y == y->parent->left)
				y->parent->left = x;
			else
				y->parent->right = x;
		}
		else
			root = x;
		if (y->color == black)
			deleteFixup(x);
		y = NULL;
		//delete(y);
		return true;
	}
	void DeleteAll(){
		root->DeleteAllWithoutColor(NIL);
		root = NIL;
	}
private:
	std::shared_ptr<Node> NIL;
	std::shared_ptr<Node> root;
};
