#pragma once

#include <iterator>
#include <memory>
#include <utility>
#include <vector>

const int kDefaultTableSize = 8;
const double kMaxLoadFactor = 1.0;
const int kRehashConstant = 2;
//having this as bucket_ in iterator says that the iterator is end one
const int kIteratorErrorBucket = -1;

template <typename Key, typename Value, typename Hash = std::hash<Key>>
class AssociativeArray {
public:
	AssociativeArray();
	AssociativeArray(int bucket_count);
	AssociativeArray(const AssociativeArray & chained_hash);
	AssociativeArray(AssociativeArray && chained_hash);
	AssociativeArray& operator=(const AssociativeArray & chained_hash);
	AssociativeArray& operator=(AssociativeArray && chained_hash);

    class Iterator: public std::iterator <std::forward_iterator_tag, std::pair<Key, Value> > {
        public:
			friend class AssociativeArray;
			Iterator(const Iterator & other);
			Iterator & operator= (const Iterator & other);
			std::pair<Key, Value> & operator*();
			const std::pair<Key, Value> & operator*() const;
			typename std::vector<std::pair<Key, Value>>::iterator & operator->();
			const typename std::vector<std::pair<Key, Value>>::iterator & operator->() const;
			Iterator& operator ++();
			Iterator operator ++(int);
			bool operator==(const Iterator& other) const;
			bool operator!=(const Iterator& other) const;
        private:
			Iterator(AssociativeArray & table,
				typename std::vector<std::pair<Key, Value>>::iterator bucket_iterator, int bucket);
			AssociativeArray & reference_table_;
			typename std::vector<std::pair<Key, Value>>::iterator bucket_iterator_;
			int bucket_;
	};

	class ConstIterator : public std::iterator <std::forward_iterator_tag, std::pair<Key, Value> > {
		public:
			friend class AssociativeArray;
			ConstIterator(const ConstIterator & other);
			ConstIterator(const Iterator & other);
			ConstIterator & operator= (const ConstIterator & other);
			const std::pair<Key, Value> & operator*() const;
			const typename std::vector<std::pair<Key, Value>>::const_iterator & operator->() const;
			ConstIterator& operator ++();
			ConstIterator& operator ++(int);
			bool operator==(const ConstIterator& other) const;
			bool operator!=(const ConstIterator& other) const;
		private:
			ConstIterator(const AssociativeArray & table,
				typename std::vector<std::pair<Key, Value>>::const_iterator bucket_iterator, int bucket);
			const AssociativeArray & reference_table_;
			typename std::vector<std::pair<Key, Value>>::const_iterator bucket_iterator_;
			int bucket_;
	};

	Value & operator[](const Key& ind);
	Value & operator[](Key && ind);
	Iterator begin();
	Iterator end();
	ConstIterator begin() const;
	ConstIterator end() const;
	ConstIterator cbegin() const;
	ConstIterator cend() const;
	Iterator find(const Key& ind);
	ConstIterator find(const Key& ind) const;
	std::pair<Iterator, bool> insert(const std::pair<Key, Value> & element);
	Iterator erase(ConstIterator pos);
	int erase(const Key& ind);
	int size() const;
private:
	//table_ is one element more than really used to get iterator to the end
	std::vector<std::vector<std::pair<Key, Value>>> table_;
	int bucket_count_;
	int num_elements_;
	//these are helper functions
	int NumOfBucket(const Key &ind);
	void Rehash();
};


template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::AssociativeArray() : 
	bucket_count_(kDefaultTableSize),
	num_elements_(0){
	table_.resize(kDefaultTableSize+1);
}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::AssociativeArray(int bucket_count): 
	num_elements_(0){
	if (bucket_count <= 0)
		bucket_count = kDefaultTableSize;
	bucket_count_ = bucket_count;
	table_.resize(bucket_count+1);
}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::AssociativeArray(const AssociativeArray & chained_hash) :
	table_(chained_hash.table_), 
	bucket_count_(chained_hash.bucket_count_),
	num_elements_(chained_hash.num_elements_){}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::AssociativeArray(AssociativeArray && chained_hash) :
	table_(std::move(table_)),
	bucket_count_(std::move(bucket_count_)),
	num_elements_(std::move(num_elements_)) {}


template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>& AssociativeArray<Key, Value, Hash>::operator=(const AssociativeArray & chained_hash) {
	table_ = chained_hash.table_;
	bucket_count_ = chained_hash.bucket_count_;
	num_elements_ = chained_hash.num_elements_;
	return *this;
}

template <typename Key, typename Value, typename Hash> 
AssociativeArray<Key, Value, Hash>& AssociativeArray<Key, Value, Hash>::operator=(AssociativeArray && chained_hash) {
	table_ = std::move(chained_hash.table_);
	bucket_count_ = std::move(chained_hash.bucket_count_);
	num_elements_ = std::move(chained_hash.num_elements_);
	return *this;
}



template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::Iterator::Iterator(AssociativeArray & table,
	typename std::vector<std::pair<Key, Value>>::iterator bucket_iterator, int bucket) :
	reference_table_(table),
	bucket_iterator_(bucket_iterator),
	bucket_(bucket) {
	int fake_bucket = reference_table_.bucket_count_;
	typename std::vector<std::pair<Key, Value>>::iterator fake_iterator = 
		reference_table_.table_[fake_bucket].begin();
	if (bucket_ < 0 || bucket_ >= fake_bucket ) {
		bucket_iterator_ = fake_iterator;
		bucket_ = kIteratorErrorBucket;
		return;
	}
	if (bucket_iterator_ != reference_table_.table_[bucket_].end()) {
		return;
	}
	++bucket_;
	//returns when the iterator pointed to valid element
	for (; bucket_ != fake_bucket; ++bucket_) {
		for (bucket_iterator_ = reference_table_.table_[bucket_].begin();
		bucket_iterator_ != reference_table_.table_[bucket_].end(); ++bucket_iterator_) {
			return;
		}
	}
	bucket_iterator_ = fake_iterator;
	bucket_ = kIteratorErrorBucket;
}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::Iterator::Iterator(const Iterator & other) :
	reference_table_(other.reference_table_),
	bucket_iterator_(other.bucket_iterator_),
	bucket_(other.bucket_) {}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator &
	AssociativeArray<Key, Value, Hash>::Iterator::operator=(const Iterator & other) {
	reference_table_ = other.reference_table_;
	bucket_iterator_ = other.bucket_iterator_;
	bucket_ = other.bucket_;
	return *this;
}


template <typename Key, typename Value, typename Hash>
std::pair<Key, Value> & AssociativeArray<Key, Value, Hash>::Iterator::operator*() {
	return *bucket_iterator_;
}

template <typename Key, typename Value, typename Hash>
const std::pair<Key, Value> & AssociativeArray<Key, Value, Hash>::Iterator::operator*() const {
	return *bucket_iterator_;
}

template <typename Key, typename Value, typename Hash>
typename std::vector<std::pair<Key, Value>>::iterator &
	AssociativeArray<Key, Value, Hash>::Iterator::operator->() {
	return bucket_iterator_;
}

template <typename Key, typename Value, typename Hash>
const typename std::vector<std::pair<Key, Value>>::iterator &
AssociativeArray<Key, Value, Hash>::Iterator::operator->() const {
	return bucket_iterator_;
}


template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator & 
	AssociativeArray<Key, Value, Hash>::Iterator::operator ++() {
	int fake_bucket = reference_table_.bucket_count_;
	typename std::vector<std::pair<Key, Value>>::iterator fake_iterator =
		reference_table_.table_[fake_bucket].begin();
	if (bucket_ < 0	|| bucket_ >= fake_bucket) {
		bucket_iterator_ = fake_iterator;
		bucket_ = kIteratorErrorBucket;
		return *this;
	}
	if (bucket_iterator_ != reference_table_.table_[bucket_].end()) {
		++bucket_iterator_;
		if (bucket_iterator_ != reference_table_.table_[bucket_].end())
			return *this;
	}
	++bucket_;
	//returns when it is pointed to valid element
	for (; bucket_ != fake_bucket; ++bucket_) {
		for (bucket_iterator_ = reference_table_.table_[bucket_].begin();
		bucket_iterator_ != reference_table_.table_[bucket_].end(); ++bucket_iterator_) {
			return *this;
		}
	}
	bucket_iterator_ = fake_iterator;
	bucket_ = kIteratorErrorBucket;
	return *this;
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
AssociativeArray<Key, Value, Hash>::Iterator::operator ++(int) {
	Iterator return_iterator = *this;
	++return_iterator;
	return return_iterator;
}


template <typename Key, typename Value, typename Hash>
bool AssociativeArray<Key, Value, Hash>::Iterator::operator==(const Iterator& other) const {
	if (&reference_table_ != &other.reference_table_)
		return false;
	if (bucket_ != other.bucket_)
		return false;
	if (bucket_ == kIteratorErrorBucket)
		return true;
	return bucket_iterator_ == other.bucket_iterator_;

}

template <typename Key, typename Value, typename Hash>
bool AssociativeArray<Key, Value, Hash>::Iterator::operator!=(const Iterator& other) const {
	return !(*this == other);
}


template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::ConstIterator::ConstIterator(const AssociativeArray & table,
	typename std::vector<std::pair<Key, Value>>::const_iterator bucket_iterator, int bucket) :
	reference_table_(table),
	bucket_iterator_(bucket_iterator),
	bucket_(bucket) {
	int fake_bucket = reference_table_.bucket_count_;
	typename std::vector<std::pair<Key, Value>>::const_iterator fake_iterator =
		reference_table_.table_[fake_bucket].cbegin();
	if (bucket_ < 0 || bucket_ >= fake_bucket) {
		bucket_iterator_ = fake_iterator;
		bucket_ = kIteratorErrorBucket;
		return;
	}
	if (bucket_iterator_ != reference_table_.table_[bucket_].cend()) {
		return;
	}
	++bucket_;
	//returns when the iterator pointed to valid element
	for (; bucket_ != fake_bucket; ++bucket_) {
		for (bucket_iterator_ = reference_table_.table_[bucket_].cbegin();
		bucket_iterator_ != reference_table_.table_[bucket_].cend(); ++bucket_iterator_) {
			return;
		}
	}
	bucket_iterator_ = fake_iterator;
	bucket_ = kIteratorErrorBucket;
}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::ConstIterator::ConstIterator(const ConstIterator & other) :
	reference_table_(other.reference_table_),
	bucket_iterator_(other.bucket_iterator_),
	bucket_(other.bucket_) {}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::ConstIterator::ConstIterator(const Iterator & other) :
	reference_table_(other.reference_table_),
	bucket_iterator_(other.bucket_iterator_),
	bucket_(other.bucket_) {}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator &
AssociativeArray<Key, Value, Hash>::ConstIterator::operator=(const ConstIterator & other) {
	reference_table_ = other.reference_table_;
	bucket_iterator_ = other.bucket_iterator_;
	bucket_ = other.bucket_;
	return *this;
}

template <typename Key, typename Value, typename Hash>
const std::pair<Key, Value> & AssociativeArray<Key, Value, Hash>::ConstIterator::operator*() const {
	return *bucket_iterator_;
}

template <typename Key, typename Value, typename Hash>
const typename std::vector<std::pair<Key, Value>>::const_iterator &
AssociativeArray<Key, Value, Hash>::ConstIterator::operator->() const {
	return bucket_iterator_;
}


template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator &
AssociativeArray<Key, Value, Hash>::ConstIterator::operator ++() {
	int fake_bucket = reference_table_.bucket_count_;
	typename std::vector<std::pair<Key, Value>>::const_iterator fake_iterator =
		reference_table_.table_[fake_bucket].cbegin();
	if (bucket_ < 0 || bucket_ >= fake_bucket) {
		bucket_iterator_ = fake_iterator;
		bucket_ = kIteratorErrorBucket;
		return *this;
	}
	if (bucket_iterator_ != reference_table_.table_[bucket_].cend()) {
		++bucket_iterator_;
		if (bucket_iterator_ != reference_table_.table_[bucket_].cend())
			return *this;
	}
	++bucket_;
	//returns when it is pointed to valid element
	for (; bucket_ != fake_bucket; ++bucket_) {
		for (bucket_iterator_ = reference_table_.table_[bucket_].cbegin();
		bucket_iterator_ != reference_table_.table_[bucket_].cend(); ++bucket_iterator_) {
			return *this;
		}
	}
	bucket_iterator_ = fake_iterator;
	bucket_ = kIteratorErrorBucket;
	return *this;
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator &
AssociativeArray<Key, Value, Hash>::ConstIterator::operator ++(int) {
	ConstIterator return_iterator = *this;
	++return_iterator;
	return return_iterator;
}


template <typename Key, typename Value, typename Hash>
bool AssociativeArray<Key, Value, Hash>::ConstIterator::operator==(const ConstIterator& other) const {
	if (&reference_table_ != &other.reference_table_)
		return false;
	if (bucket_ != other.bucket_)
		return false;
	if (bucket_ == kIteratorErrorBucket)
		return true;
	return bucket_iterator_ == other.bucket_iterator_;

}

template <typename Key, typename Value, typename Hash>
bool AssociativeArray<Key, Value, Hash>::ConstIterator::operator!=(const ConstIterator& other) const {
	return !(*this == other);
}


template <typename Key, typename Value, typename Hash>
Value & AssociativeArray<Key, Value, Hash>::operator[](const Key& ind) {
	Iterator exist = find(ind);
	if (exist!= end()) {
		return exist.bucket_iterator_->second;
	}
	Rehash();
	++num_elements_;
	int bucket = NumOfBucket(ind);
	Value empty_value;
	table_[bucket].push_back(std::make_pair(ind, empty_value));
	return (--table_[bucket].end())->second;
}

template <typename Key, typename Value, typename Hash>
Value & AssociativeArray<Key, Value, Hash>::operator[](Key && ind) {
	Iterator exist = find(ind);
	if (exist != end()) {
		return exist.bucket_iterator_->second;
	}
	Rehash();
	++num_elements_;
	int bucket = NumOfBucket(ind);
	Value empty_value;
	table_[bucket].push_back(std::make_pair(std::move(ind), empty_value));
	return (--table_[bucket].end())->second;
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
	AssociativeArray <Key, Value, Hash>::begin() {
	int num_bucket;
	typename std::vector<std::pair<Key, Value>>::iterator bucket_iterator;
	for (num_bucket = 0; num_bucket < bucket_count_; ++num_bucket) {
		for (bucket_iterator = table_[num_bucket].begin();
			bucket_iterator != table_[num_bucket].end();
			++bucket_iterator)
			return Iterator(*this, bucket_iterator, num_bucket);
	}
	bucket_iterator = table_[bucket_count_].begin();
	return Iterator(*this, bucket_iterator, kIteratorErrorBucket);
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
AssociativeArray <Key, Value, Hash>::end()  {
	typename std::vector<std::pair<Key, Value>>::iterator bucket_iterator
		= table_[bucket_count_].begin();
	return Iterator(*this, bucket_iterator, kIteratorErrorBucket);
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
AssociativeArray <Key, Value, Hash>::begin() const {
	int num_bucket;
	typename std::vector<std::pair<Key, Value>>::const_iterator bucket_iterator;
	for (num_bucket = 0; num_bucket < bucket_count_; ++num_bucket) {
		for (bucket_iterator = table_[num_bucket].cbegin();
		bucket_iterator != table_[num_bucket].cend();
			++bucket_iterator)
			return ConstIterator(*this, bucket_iterator, num_bucket);
	}
	bucket_iterator = table_[bucket_count_].cbegin();
	return ConstIterator(*this, bucket_iterator, kIteratorErrorBucket);
}


template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
AssociativeArray <Key, Value, Hash>::end() const {
	typename std::vector<std::pair<Key, Value>>::const_iterator bucket_iterator
		= table_[bucket_count_].cbegin();
	return ConstIterator(*this, bucket_iterator, kIteratorErrorBucket);
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
AssociativeArray <Key, Value, Hash>::cbegin() const{
	int num_bucket;
	typename std::vector<std::pair<Key, Value>>::const_iterator bucket_iterator;
	for (num_bucket = 0; num_bucket < bucket_count_; ++num_bucket) {
		for (bucket_iterator = table_[num_bucket].cbegin();
		bucket_iterator != table_[num_bucket].cend();
			++bucket_iterator)
			return ConstIterator(*this, bucket_iterator, num_bucket);
	}
	bucket_iterator = table_[bucket_count_].cbegin();
	return ConstIterator(*this, bucket_iterator, kIteratorErrorBucket);
}


template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
AssociativeArray <Key, Value, Hash>::cend() const{
	typename std::vector<std::pair<Key, Value>>::const_iterator bucket_iterator
		= table_[bucket_count_].cbegin();
	return ConstIterator(*this, bucket_iterator, kIteratorErrorBucket);
}


template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
AssociativeArray <Key, Value, Hash>::find(const Key& ind) {
	int bucket = NumOfBucket(ind);
	for (auto it = table_[bucket].begin();
		it != table_[bucket].end(); ++it) {
		if (it->first == ind)
			return Iterator(*this, it, bucket);
	}
	return end();
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
AssociativeArray <Key, Value, Hash>::find(const Key& ind) const {
	int bucket = NumOfBucket(ind);
	for (auto it = table_[bucket].cbegin();
	it != table_[bucket].cend(); ++it) {
		if (it->first == ind)
			return ConstIterator(*this, it, bucket);
	}
	return cend();
}


template <typename Key, typename Value, typename Hash>
std::pair<typename AssociativeArray<Key, Value, Hash>::Iterator, bool>
AssociativeArray <Key, Value, Hash>::insert(const std::pair<Key, Value> & element) {
	Iterator exist = find(element.first);
	if (exist != end())
		return std::make_pair(exist, false);
	Rehash();
	++num_elements_;
	int bucket = NumOfBucket(element.first);
	table_[bucket].push_back(element);
	return std::make_pair(Iterator(*this, --(table_[bucket].end()),bucket), true);
}

template <typename Key, typename Value, typename Hash>
int AssociativeArray <Key, Value, Hash>::erase(const Key& ind) {
	Iterator exist = find(ind);
	if (exist == end())
		return 0;
	table_[NumOfBucket(ind)].erase(exist.bucket_iterator_);
	--num_elements_;
	return 1;
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
AssociativeArray <Key, Value, Hash>::erase(ConstIterator pos) {
	if (pos.bucket_ == kIteratorErrorBucket)
		return end();
	int delta = pos.bucket_iterator_ -table_[pos.bucket_].begin();
	table_[pos.bucket_].erase(pos.bucket_iterator_);
	--num_elements_;
	return Iterator(*this, table_[pos.bucket_].begin() + delta, pos.bucket_);
}

template <typename Key, typename Value, typename Hash>
int AssociativeArray <Key, Value, Hash>::size() const {
	return num_elements_;
}



template <typename Key, typename Value, typename Hash>
int AssociativeArray <Key, Value, Hash>::NumOfBucket(const Key &ind) {
	size_t bucket = Hash()(ind);
	bucket %= bucket_count_;
	return bucket;
}

template <typename Key, typename Value, typename Hash>
void AssociativeArray <Key, Value, Hash>::Rehash() {
	double load_factor = (double)(num_elements_+1) / bucket_count_;
	if (load_factor <= kMaxLoadFactor)
		return;
	AssociativeArray <Key, Value, Hash> map_copy = *this;
	*this = AssociativeArray(kRehashConstant*bucket_count_);
	for (auto it = map_copy.begin();
	it != map_copy.end(); ++it)
		this->insert(*it);
}