#pragma once

#include <iterator>
#include <memory>
#include <utility>
#include <vector>

const int kDefaultTableSize = 32;
//this supposed to be not more than 1
const double kMaxLoadFactor = 0.7;
const int kRehashConstant = 2;
//this supposed to be coprime with 2 and default table size
const int kLinearProbe = 1;
enum States {
	kElementAbsence,
	kElementExistence
};

template <typename Key, typename Value, typename Hash = std::hash<Key>>
class AssociativeArray {
public:
	AssociativeArray();
	AssociativeArray(int table_size);
	AssociativeArray(const AssociativeArray & open_addr_hash);
	AssociativeArray(AssociativeArray && open_addr_hash);
	AssociativeArray& operator=(const AssociativeArray & open_addr_hash);
	AssociativeArray& operator=(AssociativeArray && open_addr_hash);

    class Iterator: public std::iterator <std::forward_iterator_tag, std::pair<Key, Value> > {
        public:
			friend class AssociativeArray;
			Iterator(const Iterator & other);
			Iterator & operator= (const Iterator & other);
			std::pair<Key, Value> & operator*() const;
			std::pair<Key, Value> * operator->() const;
			Iterator& operator ++();
			Iterator operator ++(int);
			bool operator==(const Iterator& other) const;
			bool operator!=(const Iterator& other) const;
        private:
			Iterator(AssociativeArray & table,
				const typename std::vector<std::pair<std::pair<Key, Value>, States>>::iterator & iterator);
			AssociativeArray & reference_table_;
			typename std::vector<std::pair<std::pair<Key, Value>, States>>::iterator iterator_;
	};

	class ConstIterator : public std::iterator <std::forward_iterator_tag, std::pair<Key, Value> > {
		public:
			friend class AssociativeArray;
			ConstIterator(const Iterator & other);
			ConstIterator(const ConstIterator & other);
			ConstIterator & operator= (const ConstIterator & other);
			const std::pair<Key, Value> & operator*() const;
			const std::pair<Key, Value> * operator->() const;
			ConstIterator& operator ++();
			ConstIterator operator ++(int);
			bool operator==(const ConstIterator& other) const;
			bool operator!=(const ConstIterator& other) const;
		private:
			ConstIterator(const AssociativeArray & table,
				const typename std::vector<std::pair<std::pair<Key, Value>, States>>::const_iterator & iterator);
			const AssociativeArray & reference_table_;
			typename std::vector<std::pair<std::pair<Key, Value>, States>>::const_iterator iterator_;
	};
	Value & operator[](const Key& ind);
	Value & operator[](Key && ind);
	Iterator begin();
	Iterator end();
	ConstIterator begin() const;
	ConstIterator end() const;
	ConstIterator cbegin() const;
	ConstIterator cend() const;
	Iterator find(const Key& ind);
	ConstIterator find(const Key& ind) const;
	std::pair<Iterator, bool> insert(const std::pair<Key, Value> & element);
	std::pair<Iterator, bool> insert(std::pair<Key, Value> && element);
	Iterator erase(ConstIterator pos);
	int erase(const Key& ind);
	int size() const;
private:
	std::vector<std::pair<std::pair<Key, Value>, States>> table_;
	int num_elements_;
	//these are helper functions
	int GetHash(const Key &ind) const;
	void Rehash();
	void HelpToErase(int init_pos);
};


template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::AssociativeArray() :
	num_elements_(0),
	table_(kDefaultTableSize) {
	for (auto it = table_.begin(); it != table_.end(); ++it)
		it->second = kElementAbsence;
}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::AssociativeArray(int table_size): 
	num_elements_(0){
	if (table_size <= 0)
		table_size = kDefaultTableSize;
	table_.resize(table_size);
	for (auto it = table_.begin(); it != table_.end(); ++it)
		it->second = kElementAbsence;
}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::AssociativeArray(const AssociativeArray & open_addr_hash):
table_(open_addr_hash.table_), num_elements_(open_addr_hash.num_elements_) {}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::AssociativeArray(AssociativeArray && open_addr_hash) :
	table_(std::move(open_addr_hash.table_)),
	num_elements_(std::move(open_addr_hash.num_elements_)) {}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash> &
AssociativeArray<Key, Value, Hash>::operator=(const AssociativeArray & open_addr_hash) {
	table_ = open_addr_hash.table_;
	num_elements_ = open_addr_hash.num_elements_;
	return *this;
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash> &
AssociativeArray<Key, Value, Hash>::operator=(AssociativeArray && open_addr_hash) {
	table_ = std::move(open_addr_hash.table_);
	num_elements_ = std::move(open_addr_hash.num_elements_);
	return *this;
}


template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::Iterator::Iterator(AssociativeArray & table,
	const typename std::vector<std::pair<std::pair<Key, Value>, States>>::iterator & iterator):
	reference_table_(table),
	iterator_(iterator){
	while (iterator_ != reference_table_.table_.end() &&
		iterator_->second != kElementExistence)
		++iterator_;
}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::Iterator::Iterator(const Iterator & other):
	reference_table_(other.reference_table_),
	iterator_(other.iterator_) {}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator & 
	AssociativeArray<Key, Value, Hash>::Iterator::operator= (const Iterator & other) {
	reference_table_ = other.reference_table_;
	iterator_ = other.iterator_;
	return *this;
}

template <typename Key, typename Value, typename Hash>
std::pair<Key, Value> & AssociativeArray<Key, Value, Hash>::Iterator::operator*() const {
	return iterator_->first;
}

template <typename Key, typename Value, typename Hash>
std::pair<Key, Value> * AssociativeArray<Key, Value, Hash>::Iterator::operator->() const {
	return &(iterator_->first);
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator&
	AssociativeArray<Key, Value, Hash>::Iterator::operator++() {
	if (iterator_ != reference_table_.table_.end() &&
		iterator_->second == kElementExistence)
		++iterator_;
	while (iterator_ != reference_table_.table_.end()) {
		if (iterator_->second == kElementExistence)
			return *this;
		++iterator_;
	}
	return *this;
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
	AssociativeArray<Key, Value, Hash>::Iterator::operator++(int) {
	Iterator return_iterator = *this;
	++*this;
	return return_iterator;
}

template <typename Key, typename Value, typename Hash>
bool AssociativeArray<Key, Value, Hash>::Iterator::operator==(const Iterator& other) const {
	return iterator_ == other.iterator_;
}

template <typename Key, typename Value, typename Hash>
bool AssociativeArray<Key, Value, Hash>::Iterator::operator!=(const Iterator& other) const {
	return iterator_ != other.iterator_;
}



template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::ConstIterator::ConstIterator(const AssociativeArray & table,
	const typename std::vector<std::pair<std::pair<Key, Value>, States>>::const_iterator & iterator) :
	reference_table_(table),
	iterator_(iterator) {
	while (iterator_ != reference_table_.table_.cend() &&
		iterator_->second != kElementExistence)
		++iterator_;
}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::ConstIterator::ConstIterator(const Iterator & other) :
	reference_table_(other.reference_table_),
	iterator_(other.iterator_) {}

template <typename Key, typename Value, typename Hash>
AssociativeArray<Key, Value, Hash>::ConstIterator::ConstIterator(const ConstIterator & other) :
	reference_table_(other.reference_table_),
	iterator_(other.iterator_) {}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator &
AssociativeArray<Key, Value, Hash>::ConstIterator::operator= (const ConstIterator & other) {
	reference_table_ = other.reference_table_;
	iterator_ = other.iterator_;
	return *this;
}

template <typename Key, typename Value, typename Hash>
const std::pair<Key, Value> & AssociativeArray<Key, Value, Hash>::ConstIterator::operator*() const {
	return iterator_->first;
}

template <typename Key, typename Value, typename Hash>
const std::pair<Key, Value> * AssociativeArray<Key, Value, Hash>::ConstIterator::operator->() const {
	return &(iterator_->first);
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator&
AssociativeArray<Key, Value, Hash>::ConstIterator::operator++() {
	if (iterator_ != reference_table_.table_.cend() &&
		iterator_->second == kElementExistence)
		++iterator_;
	while (iterator_ != reference_table_.table_.cend()) {
		if (iterator_->second == kElementExistence)
			return *this;
		++iterator_;
	}
	return *this;
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
AssociativeArray<Key, Value, Hash>::ConstIterator::operator++(int) {
	ConstIterator return_iterator = *this;
	++*this;
	return return_iterator;
}

template <typename Key, typename Value, typename Hash>
bool AssociativeArray<Key, Value, Hash>::ConstIterator::operator==(const ConstIterator& other) const {
	return iterator_ == other.iterator_;
}

template <typename Key, typename Value, typename Hash>
bool AssociativeArray<Key, Value, Hash>::ConstIterator::operator!=(const ConstIterator& other) const {
	return iterator_ != other.iterator_;
}


template <typename Key, typename Value, typename Hash>
Value & AssociativeArray<Key, Value, Hash>::operator[](const Key& ind) {
	Iterator exist = find(ind);
	if (exist != end()) {
		return exist->second;
	}
	Value empty_value;
	Iterator new_element = insert(std::make_pair(ind, empty_value)).first;
	return new_element->second;
}

template <typename Key, typename Value, typename Hash>
Value & AssociativeArray<Key, Value, Hash>::operator[](Key&& ind) {
	Iterator exist = find(ind);
	if (exist != end()) {
		return exist->second;
	}
	Value empty_value;
	Iterator new_element = insert(std::make_pair(std::move(ind), empty_value)).first;
	return new_element->second;
}


template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
	AssociativeArray<Key, Value, Hash>::begin() {
	return Iterator(*this, table_.begin());
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
	AssociativeArray<Key, Value, Hash>::end() {
	return Iterator(*this, table_.end());
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
	AssociativeArray<Key, Value, Hash>::begin() const {
	return ConstIterator(*this, table_.cbegin());
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
	AssociativeArray<Key, Value, Hash>::end() const {
	return ConstIterator(*this, table_.cend());
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
	AssociativeArray<Key, Value, Hash>::cbegin() const {
	return ConstIterator(*this, table_.cbegin());
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
	AssociativeArray<Key, Value, Hash>::cend() const {
	return ConstIterator(*this, table_.cend());
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
	AssociativeArray<Key, Value, Hash>::find(const Key& ind) {
	int pos = GetHash(ind);
	for (int i = pos; table_[i].second != kElementAbsence; 
		i = (i + kLinearProbe) % table_.size()) {
		if (table_[i].first.first == ind)
			return Iterator(*this, table_.begin() + i);
	}
	return end();
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::ConstIterator
	AssociativeArray<Key, Value, Hash>::find(const Key& ind) const{
	int pos = GetHash(ind);
	for (int i = pos; table_[i].second != kElementAbsence;
	i = (i + kLinearProbe) % table_.size()) {
		if (table_[i].first.first == ind)
			return ConstIterator(*this, table_.begin() + i);
	}
	return cend();
}

template <typename Key, typename Value, typename Hash>
std::pair<typename AssociativeArray<Key, Value, Hash>::Iterator, bool>
	AssociativeArray<Key, Value, Hash>::insert(const std::pair<Key, Value> & element) {
	Iterator exist = find(element.first);
	if (exist != end())
		return std::make_pair(exist, false);
	Rehash();
	++num_elements_;
	int pos = GetHash(element.first);
	for (; table_[pos].second != kElementAbsence;
		pos = (pos + kLinearProbe) % table_.size()) {
	}
	table_[pos] = std::make_pair(element, kElementExistence);
	return std::make_pair(Iterator(*this, table_.begin() + pos), true);
}

template <typename Key, typename Value, typename Hash>
std::pair<typename AssociativeArray<Key, Value, Hash>::Iterator, bool>
	AssociativeArray<Key, Value, Hash>::insert(std::pair<Key, Value> && element) {
	Iterator exist = find(element.first);
	if (exist != end())
		return std::make_pair(exist, false);
	Rehash();
	++num_elements_;
	int pos = GetHash(element.first);
	for (; table_[pos].second != kElementAbsence;
	pos = (pos + kLinearProbe) % table_.size()) {
	}
	table_[pos] = std::make_pair(std::move(element), kElementExistence);
	return std::make_pair(Iterator(*this, table_.begin() + pos), true);
}

template <typename Key, typename Value, typename Hash>
int AssociativeArray<Key, Value, Hash>::erase(const Key & ind) {
	Iterator exist = find(ind);
	if (exist == end())
		return 0;
	int init_pos = exist.iterator_ - table_.begin();
	HelpToErase(init_pos);
	--num_elements_;
	return 1;
}

template <typename Key, typename Value, typename Hash>
typename AssociativeArray<Key, Value, Hash>::Iterator
	AssociativeArray<Key, Value, Hash>::erase(ConstIterator pos) {
	if (pos.iterator_->second == kElementAbsence)
		return end();
	int init_pos = pos.iterator_ - table_.cbegin();
	HelpToErase(init_pos);
	--num_elements_;
	return Iterator(*this, table_.begin()+init_pos);
}

template <typename Key, typename Value, typename Hash>
void AssociativeArray<Key, Value, Hash>::HelpToErase(int init_pos) {
	int pos = (init_pos + kLinearProbe) % table_.size();
	while (table_[pos].second == kElementAbsence ||
		GetHash(table_[pos].first.first) != GetHash(table_[init_pos].first.first)) {
		if (table_[pos].second == kElementAbsence) {
			table_[init_pos].second = kElementAbsence;
			return;
		}
		pos = (pos + kLinearProbe) % table_.size();
	}
	table_[init_pos] = table_[pos];
	HelpToErase(pos);
}

template <typename Key, typename Value, typename Hash>
int AssociativeArray<Key, Value, Hash>::size() const {
	return num_elements_;
}

template <typename Key, typename Value, typename Hash>
int AssociativeArray<Key, Value, Hash>::GetHash(const Key &ind) const {
	size_t hash_ind = Hash()(ind);
	hash_ind %= table_.size();
	return hash_ind;
}

template <typename Key, typename Value, typename Hash>
void AssociativeArray<Key, Value, Hash>::Rehash() {
	if ((double)(num_elements_ + 1) / table_.size() < kMaxLoadFactor)
		return;
	AssociativeArray map_copy = *this;
	*this = AssociativeArray(kRehashConstant * map_copy.table_.size());
	for (auto it = map_copy.begin();
	it != map_copy.end(); ++it)
		this->insert(*it);
}