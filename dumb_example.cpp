#include "chained_hash.h"
#include <utility>
#include <iostream>
#include <string>

template <typename Key, typename Value, typename Hash>
void PrintContents(const AssociativeArray<Key, Value, Hash> & example) {
	std::cout << "contents:" << std::endl;
	for (auto it = example.cbegin(); it != example.cend(); ++it) {
		std::cout << " " << it->first << "=>" << it->second << std::endl;
	}
	std::cout << "example contains " << example.size() << " elements " << std::endl;
}

int main() {
	AssociativeArray<int, std::string> example;
	example.insert(std::make_pair(1, "ferret"));
	example.insert(std::make_pair(2, "swan"));
	example.insert(std::make_pair(3, "goat"));
	example.insert(std::make_pair(4, "phoenix"));
	example.insert(std::make_pair(4, "another phoenix"));
	example.insert(std::make_pair(5, "fox"));
	bool ok = example.insert(std::make_pair(1, "another ferret")).second;
	std::cout << "inserting 1 -> \"another phoenix\" "
		<< (ok ? "succeeded" : "failed") << std::endl;
	PrintContents(example);
	example[4] = "stag";
	example[6] = "otter";
	PrintContents(example);
	std::cout << example.erase(example.cbegin())->first << std::endl;
	example.erase(4);
	PrintContents(example);
}