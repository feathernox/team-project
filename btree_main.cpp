#include <exception>
#include "btree.h"

std::shared_ptr<Btree<int, std::string>> tree = std::make_shared<Btree<int, std::string>>();

void fillTree(std::shared_ptr<Btree<int, std::string>> tree) {
    tree->insert(16, "one");
    tree->print();
    std::cout << std::endl;
    tree->insert(18, "two");
    tree->print();
    std::cout << std::endl;
    tree->insert(20, "three");
    tree->print();
    std::cout << std::endl;
    tree->insert(21, "four");
    tree->print();
    std::cout << std::endl;
    tree->insert(14, "five");
    tree->print();
    std::cout << std::endl;
    tree->insert(24, "six");
    tree->print();
    std::cout << std::endl;
    tree->insert(17, "seven");
    tree->print();
    std::cout << std::endl;
    tree->insert(1, "eight");
    tree->print();
    std::cout << std::endl;
    tree->insert(5, "nine");
    tree->print();
    std::cout << std::endl;
    tree->insert(11, "ten");
    tree->print();
    std::cout << std::endl;
    tree->insert(19, "eleven");
    tree->print();
    std::cout << std::endl;
    tree->insert(6, "twelve");
    tree->print();
    std::cout << std::endl;
    tree->insert(25, "thirteen");
    tree->print();
    std::cout << std::endl;
}

int main(){
    try {
        fillTree(tree);
    }
    catch (std::logic_error k) {
        std::cout << k.what() << std::endl;
    }

    auto it = tree->begin();
    for (int i = 0; i + 1 < tree->size(); i++) {
        std::cout << it.first() << std::endl;
        try {
            ++it;
        }
        catch(std::logic_error l) {
            std::cout << l.what();
        }
    }
    std::cout << it.first() << std::endl;
    std::cout << "Value of key 16" << std::endl;
    std::cout << (*tree)[16] << std::endl;
    try {
        tree->erase(24);
        tree->print();
        std::cout << std::endl;
         std::cout << std::endl;
        tree->erase(20);
        tree->print();
        std::cout << std::endl;
        std::cout << std::endl;
    }
    catch(std::logic_error k) {
        std::cout << k.what() << std::endl;
    }
    std::cout << "Is key 18 here? " << std::endl;
    bool res = tree->find(18);
    std::cout << res << std::endl;

    return 0;
}

