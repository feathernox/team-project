#include "Red-Black-Tree.h"
#include <string>

int main(){
	RBTree<int, std::string> tree;
	tree.print();
	tree.insert(std::pair<int, std::string>(1, "Aloha"));
	tree.insert(std::pair<int, std::string>(2, "DAD"));
	tree.insert(std::pair<int, std::string>(3, "My friend is cat"));
	tree.insert(std::pair<int, std::string>(7, "I need an excellent mark"));
	tree.insert(std::pair<int, std::string>(9, "I hate programming"));
	tree.print();
	std::cout << std::endl;
	std::shared_ptr<RBTree<int, std::string>::Node> a = tree.min();
	std::shared_ptr<RBTree<int, std::string>::Node> b = tree.max();
	a->print();
	a->second() = "Hi";
	std::cout << std::endl;
	b->print();
	b->second() = "I fall into the dark";
	std::cout << std::endl;
	RBTree<int, std::string>::iterator i;
	for (i = tree.begin(); i != tree.end(); i++){
		std::cout << i->first() << " " << i->second() << "\n";
	}
	std::cout << std::endl;
	i = tree.find(3);
	i->print();
	std::cout << std::endl;
	RBTree<int, std::string>::reverse_iterator rit;
	for (rit = tree.rbegin(); rit != tree.end(); ++rit)
		rit->print();
	std::cout << std::endl;
	RBTree<int, std::string> tmp(tree);
	tmp[9] = "KOK";
	tmp[4] = "Allah";
	tmp.erase(2);
	tree.print();
	std::cout << std::endl;
	tmp.print();
	std::cout << std::endl;
	tmp = tree;
	tree.print();
	std::cout << std::endl;
	tmp.print();
	std::cout << std::endl;
	return 0;
}
